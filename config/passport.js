const LocalStrategy = require('passport-local').Strategy;
const User = require('../models').User;

module.exports = (passport) => {
	passport.serializeUser((user, done) => {
		done(null, user.id)
	});

	passport.deserializeUser((id, done) => {
		User.findOne({
			where: {
				id: id
			}
		})
		.then(user => done(null, user))
		.catch(error => done(error));
	});

	passport.use('local-signup', new LocalStrategy({
		usernameField: 'username',
		passwordField: 'password',
		passReqToCallback: true
	},
	(req, username, password, done) => {
		process.nextTick(() => {
			User.findOrCreate({
				where: {
					username: username
				},
				defaults: {
					password: User.generateHash(password)
				}
			})
			.spread((user, created) => {
				return done(null, user);
			})
			.catch(error => {return done(error)});
		})
	}));

	passport.use('local-login', new LocalStrategy({
		usernameField: 'username',
		passwordField: 'password',
		passReqToCallback: true
	},
	(req, username, password, done) => {
		User.findOne({
			where: {
				username: username
			}
		})
		.then(user => {
			if(!user){	
				return done(null, false);
			}

			if (!user.validPassword(password)){
				return done(null, false);
			}
			return done(null, user);
		})
		.catch(error => {return done(error)})
	}));
}