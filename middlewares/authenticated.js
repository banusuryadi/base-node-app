'use strict';

module.exports = (req, res, next) => {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the login page
    req.session.returnTo = req.originalUrl;
    res.redirect('/login');
}