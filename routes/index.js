'use strict';
const auth = require('./auth');
const web = require('./web');
const profile = require('./profile');
// const admin = require('./admin');
const authenticated = require('../middlewares/authenticated');

module.exports = (app, passport) => {
	app.use('/', auth);
	app.use('/', web);

	app.all('*', authenticated);
	// app.use('/admin', admin);
	app.use('/profile', profile);
}