const express = require('express');
const router = express.Router();
const passport = require('passport');

router.get('/login', (req, res, next) => {
  res.render('login.twig');
});

router.get('/signup', (req, res, next) => {
  res.render('signup.twig');
});

router.post('/signup', passport.authenticate('local-signup', {
	successRedirect:'/',
	failureRedirect:'/signup',
}));

router.post('/login', passport.authenticate('local-login', {
	successReturnToOrRedirect: '/', 
	failureRedirect: '/login'
}));

router.get('/logout', (req, res) => {
	req.logout();
	res.redirect('/');
});

module.exports = router