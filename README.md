# base-node-app
## A common base nodejs web app

* [Express 4.x](http://expressjs.com/)
* [MySQL 2](https://www.npmjs.com/package/mysql2)
* [Sequelize 4.x](http://docs.sequelizejs.com/)
* [Twig](https://twig.symfony.com/)
* [Passport](http://www.passportjs.org/)

### Install

* Clone
* `$ npm install`
* `$ npm start`
* `$ sequelize init`
* Edit config.json file

To generate user table:
* `$ sequelize model:generate --name User --attribute username:string,password:string`
* Replace `models/user.js` with:
```js
    'use strict';
    const bcrypt = require('bcrypt');

    module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('User', {
        username: DataTypes.STRING,
        password: DataTypes.STRING
    }, {});
    User.associate = (models) => {
        // associations can be defined here
    };

    User.generateHash = (password) => {
        return bcrypt.hashSync(password, 10)
    }

    User.prototype.validPassword = function(password) {
        return bcrypt.compareSync(password, this.password);
    }
    return User;
    };
```
or something like that
* `$ sequelize db:migrate`

https://stackoverflow.com/questions/34258938/sequelize-classmethods-vs-instancemethods